# LRTP Input Map

Interactive input map for the [Long Range Transportation
Plan](https://lrtp.cuuats.org/) using [Web Map
GL](https://gitlab.com/ccrpc/webmapgl).

## License
LRTP Input Map is available under the terms of the [BSD 3-clause
license](https://gitlab.com/ccrpc/transportation-voices-2045/blob/master/LICENSE.md).
