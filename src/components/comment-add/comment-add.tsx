import { Component, Prop, State } from '@stencil/core';
import { _t } from '../i18n/i18n';

@Component({
  tag: 'lrtp-comment-add'
})
export class CommentAdd {

  @State() visible: boolean = true;

  @Prop({connect: 'gl-draw-controller'}) drawCtrl!:
    HTMLGlDrawControllerElement;
  @Prop({connect: 'gl-form-controller'}) formCtrl!:
    HTMLGlFormControllerElement;
  @Prop({connect: 'gl-rest-controller'}) restCtrl!:
    HTMLGlRestControllerElement;
  @Prop({connect: 'ion-toast-controller'}) toastCtrl!:
    HTMLIonToastControllerElement;

  @Prop() label: string;
  @Prop() schema: string;
  @Prop() token: string;
  @Prop() toolbarLabel: string;
  @Prop() url: string;

  async start() {
    this.visible = false;
    let fc = await this.drawCtrl.create(null, {
      toolbarLabel: this.toolbarLabel
    });

    if (!fc || !fc['features'].length) return this.end();

    let feature = await this.formCtrl.create(fc['features'][0], {
      label: this.label,
      schema: this.schema,
      translate: true
    });

    if (!feature) return this.end();

    let ok = false;
    try {
      let res = await this.restCtrl.create(feature, {
        url: this.url,
        token: this.token
      });
      ok = res.ok;
    } finally {
      this.showToast(ok, feature);
      this.end();
    }
  }

  end() {
    this.visible = true;
  }

  async showToast(success: boolean, feature?: any) {
    let message;
    if (success) {
      let desc = feature.properties.comment_description;
      message = (!desc || desc == '') ?
        _t('lrtp.app.comment.added') : _t('lrtp.app.comment.moderation');
    } else {
      message = _t('lrtp.app.comment.error');
    }

    let options = {
      message: message,
      duration: 3000
    };

    let toast = await this.toastCtrl.create(options);
    await toast.present();
    return toast;
  }

  render() {
    if (this.visible) return (
      <ion-fab-button onClick={() => this.start()}>
        <ion-icon name="add"></ion-icon>
      </ion-fab-button>
    );
  }
}
