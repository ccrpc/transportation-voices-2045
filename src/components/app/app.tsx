import { Component, Element, Listen, Prop } from '@stencil/core';
import Driver from 'driver.js';
import { _t } from '../i18n/i18n';


@Component({
  styleUrls: [
    '../../../node_modules/driver.js/dist/driver.min.css',
    'app.css'
  ],
  tag: 'lrtp-app'
})
export class App {
  surveyCtrl?: HTMLLrtpSurveyControllerElement;
  drawer?: HTMLGlDrawerElement;
  map?: HTMLGlMapElement;

  @Element() el: HTMLLrtpAppElement;

  @Prop({connect: 'lrtp-survey-controller'}) lazySurveyCtrl!:
    HTMLLrtpSurveyControllerElement;

  @Prop() allowInput: boolean = true;
  @Prop() bbox: string;
  @Prop() commentUrl: string;
  @Prop() forwardGeocodeUrl: string;
  @Prop() likeUrl: string;
  @Prop() multiuser: boolean = false;
  @Prop() schemaUrl: string;
  @Prop() styleUrl: string;
  @Prop() surveyUrl: string;
  @Prop() token: string;

  async componentWillLoad() {
    if (this.multiuser) localStorage.clear();
  }

  async componentDidLoad() {
    this.surveyCtrl = await this.lazySurveyCtrl.componentOnReady();

    let shown = localStorage.getItem('lrtp.intro') === 'true';
    if (!shown) {
      localStorage.setItem('lrtp.intro', 'true');
      // TODO: Figure out a better way to determine when the feature list
      // is loaded.
      setTimeout(() => this.showIntro(), 1000);
    }
  }

  closeDrawer() {
    let drawer = this.el.querySelector('gl-drawer');
    if (drawer.open) drawer.toggle();
  }

  @Listen('body:glFeatureClick')
  async handleClick(e: CustomEvent) {
    const features = e.detail.features;
    if (!features || !features.length) return
    const feature = features[0];
    if (feature.layer.id === 'lrtp:comment') {
      this.handleCommentClick();
    } else {
      const zoom = await this.map.map.getZoom();
      const maxZoom = await this.map.map.getMaxZoom();
      this.map.map.easeTo({
        center: feature.geometry.coordinates,
        zoom: (zoom + 1 <= maxZoom) ? zoom + 1 : maxZoom,
        duration: 500
      });
    }
  }

  async openSurvey() {
    let alert = await this.surveyCtrl.create();
    return await alert.present();
  }

  async handleCommentClick() {
    this.closeDrawer();
  }

  showIntro() {
    let driver = new Driver({
      animate: false,
      padding: 0,
      nextBtnText: _t('lrtp.app.intro.next'),
      prevBtnText: _t('lrtp.app.intro.prev'),
      doneBtnText: _t('lrtp.app.intro.done'),
      closeBtnText: _t('lrtp.app.intro.close')
    });

    let steps = [
      {
        element: 'gl-map',
        popover: {
          title: _t('lrtp.app.intro.welcome.title'),
          description: _t('lrtp.app.intro.welcome.text', {
            app_label: '<strong>' + _t('lrtp.app.label') + '</strong>'
          })
        }
      },
      {
        element: 'lrtp-comment-detail',
        popover: {
          title: _t('lrtp.app.intro.view.title'),
          description: _t('lrtp.app.intro.view.text')
        }
      },
      {
        element: '.lrtp-locate-button',
        popover: {
          title: _t('lrtp.app.intro.locate.title'),
          description: _t('lrtp.app.intro.locate.text'),
          position: 'left'
        }
      }
    ];

    if (this.allowInput) steps.push(
      {
        element: 'gl-like-button',
        popover: {
          title: _t('lrtp.app.intro.like.title'),
          description: _t('lrtp.app.intro.like.text'),
          position: 'left'
        }
      },
      {
        element: 'ion-fab',
        popover: {
          title: _t('lrtp.app.intro.add.title'),
          description: _t('lrtp.app.intro.add.text'),
          position: 'left'
        }
      },
      {
        element: '.lrtp-survey-button',
        popover: {
          title: _t('lrtp.app.intro.survey.title'),
          description: _t('lrtp.app.intro.survey.text'),
          position: 'left'
        }
      }
    );

    driver.defineSteps(steps);
    driver.start();
  }

  render() {
    let bbox = this.bbox.split(',').map((c) => parseFloat(c));

    let surveyButton = (this.allowInput) ?
      <ion-button slot="end-buttons" class="lrtp-survey-button"
        onClick={() => this.openSurvey()}>
        <ion-icon slot="icon-only" name="bulb"></ion-icon>
      </ion-button> : null;

    let addButton = (this.allowInput) ?
      <ion-fab vertical="bottom" horizontal="end">
        <lrtp-comment-add
          url={this.commentUrl}
          token={this.token} onClick={() => this.closeDrawer()}
          schema={this.schemaUrl} label={_t('lrtp.app.comment.add')}
          toolbarLabel={_t('lrtp.app.comment.location')}>
        </lrtp-comment-add>
      </ion-fab> : null;

    return ([
      <gl-app label={_t('lrtp.app.label')} menu={false}>
        <gl-fullscreen slot="start-buttons"></gl-fullscreen>
        <gl-basemaps slot="start-buttons"></gl-basemaps>
        {surveyButton}
        <gl-drawer-toggle slot="end-buttons" icon="chatbubbles">
        </gl-drawer-toggle>
        <lrtp-address-search forwardGeocodeUrl={this.forwardGeocodeUrl}
            bbox={bbox as any}></lrtp-address-search>
        <gl-map ref={(r: HTMLGlMapElement) => this.map = r}
            longitude={-88.228878} latitude={40.110319} zoom={12} maxzoom={22}>
          <gl-style url={this.styleUrl} id="lrtp"
            clickableLayers={['cluster', 'comment']}
            name={_t('lrtp.app.comment.label')}
            enabled={true} token={this.token}>
            <gl-popup layers={['comment']} component='lrtp-comment-detail'
              componentOptions={{
                allowLike: false,
                popup: true
              }}>
            </gl-popup>
          </gl-style>
          <gl-style url="https://maps.ccrpc.org/basemaps/imagery/style.json"
            basemap={true}
            thumbnail="https://maps.ccrpc.org/tiles/imagery/13/2087/3098.png"
            name={_t('lrtp.app.basemap.imagery')} enabled={false}></gl-style>
          <gl-style url="https://maps.ccrpc.org/basemaps/hybrid/style.json"
            basemap={true}
            thumbnail="https://maps.ccrpc.org/tiles/imagery/13/2087/3098.png"
            name={_t('lrtp.app.basemap.hybrid')} enabled={true}></gl-style>
        </gl-map>
        {addButton}
        <gl-drawer slot="after-content" open={true}
            drawer-title={_t('lrtp.app.comment.label')}
            ref={(r: HTMLGlDrawerElement) => this.drawer = r}>
          <gl-feature-list styleId="lrtp" sourceId="comments" item={false}
            component="lrtp-comment-detail"
            componentOptions={{
              allowLike: false,
              likeUrl: this.likeUrl,
              token: this.token}}
            orderBy="_created" order="desc">
          </gl-feature-list>
        </gl-drawer>
        <gl-draw-toolbar slot="footer"></gl-draw-toolbar>
      </gl-app>
    ]);
  }
}
